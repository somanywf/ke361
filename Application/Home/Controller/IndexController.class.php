<?php
namespace Home\Controller;
use Home\Model;
use Home\Model\GoodsModel;
class IndexController extends HomeController {

    public function index() {
        $list = $this->lists(D('Ad'));
        $this->assign('banner',$list);
     
        $this->assign('total_page',ceil(D('Document')->listCount()/12));
        $this->assign('totalPage', $totalPage);
        $this->assign('flink', $flink);
        $this->setSiteTitle('首页');
        $this->display();
    }
    public function ajaxNews(){
        $Document = D('Document');
        $where = array('status'=>1);
        
        $list= $this->lists($Document,$where,'create_time DESC');
     
        $this->assign('_list',$list);
        $result['p']=I('get.p')+1;
	    $result['content']=$this->fetch();
	    $result['errno']=0;
	    $this->ajaxReturn($result);
        
    }
}
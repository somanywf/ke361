<?php
namespace Home\Model;

use Think\Model;

class TopicModel extends Model
{
    public function info($id){
        $where['id'] = $id;
        return $this->where($where)->find();
    }
}

?>